package com.creator.creatorlib.manager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.creator.creatorlib.R;

public class CreatorMessenger {
	private static CreatorMessenger mInstance = null;

	public static CreatorMessenger getInstance() {
		if (mInstance == null) {
			mInstance = new CreatorMessenger();
		}
		return mInstance;
	}

	private Activity mActivity;
	private boolean isProgressDialogRunning = false;
	boolean isForbiddenToAskAgain = false;

	Dialog mDialog;
	TextView textView;

	public void showMessage(Activity activity, String title, String msg) {
		if (isProgressDialogRunning) {
			hideProgressMessage();
		}
		mActivity = activity;
		mDialog = null;
		AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
		builder.setMessage(Html.fromHtml(msg)).setTitle(title);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// User clicked OK button
				dialog.dismiss();
			}
		});

		AlertDialog dialog = builder.create();

		WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
		lp.dimAmount = 0.0f;
		dialog.getWindow().setAttributes(lp);
		dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

		try {
			dialog.show();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void showMessage(Activity activity, String title, String msg,
			String buttonTitle) {
		if (isProgressDialogRunning) {
			hideProgressMessage();
		}
		mActivity = activity;
		AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
		builder.setMessage(Html.fromHtml(msg)).setTitle(title);
		builder.setPositiveButton(buttonTitle,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// User clicked OK button
						dialog.dismiss();
					}
				});

		AlertDialog dialog = builder.create();

		WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
		lp.dimAmount = 0.0f;
		dialog.getWindow().setAttributes(lp);
		dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

		try {
			dialog.show();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void showProgressMessage(Activity activity, String msg) {
		if (isProgressDialogRunning) {
			hideProgressMessage();
		}
		isProgressDialogRunning = true;
		mActivity = activity;
		mDialog = new Dialog(mActivity,
				android.R.style.Theme_Translucent_NoTitleBar);
		// mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialog.setContentView(R.layout.dialog_loading);

		textView = (TextView) mDialog.findViewById(R.id.textView);
		textView.setText(Html.fromHtml(msg));

		WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
		lp.dimAmount = 0.5f;
		mDialog.getWindow().setAttributes(lp);
		mDialog.getWindow()
				.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

		mDialog.setCancelable(false);
		mDialog.setCanceledOnTouchOutside(false);
		try {
			mDialog.show();
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Error ex) {
			ex.printStackTrace();
		}

	}

	public void hideMessage() {
		if (mDialog != null)
			mDialog.dismiss();
	}

	public void hideProgressMessage() {
		isProgressDialogRunning = false;
		try {
			if (mDialog != null)
				mDialog.dismiss();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void showTwoButtonMessage(Activity activity, String title,
			String msg, DialogInterface.OnClickListener positiveListener,
			DialogInterface.OnClickListener negativeListener) {
		if (isProgressDialogRunning) {
			hideProgressMessage();
		}
		mActivity = activity;
		AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
		builder.setMessage(Html.fromHtml(msg)).setTitle(title);
		builder.setPositiveButton("OK", positiveListener);
		builder.setNegativeButton("Cancel", negativeListener);

		AlertDialog dialog = builder.create();

		WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
		lp.dimAmount = 0.0f;
		dialog.getWindow().setAttributes(lp);
		dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

		try {
			dialog.show();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void showTwoButtonMessage(Activity activity, String title,
			String msg, String positiveButtonCaption,
			String negativeButtonCaption,
			DialogInterface.OnClickListener positiveListener,
			DialogInterface.OnClickListener negativeListener) {
		if (isProgressDialogRunning) {
			hideProgressMessage();
		}
		mActivity = activity;
		AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
		builder.setMessage(Html.fromHtml(msg)).setTitle(title);
		builder.setPositiveButton(positiveButtonCaption, positiveListener);
		builder.setNegativeButton(negativeButtonCaption, negativeListener);

		AlertDialog dialog = builder.create();

		WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
		lp.dimAmount = 0.0f;
		dialog.getWindow().setAttributes(lp);
		dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

		try {
			dialog.show();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void showOneButtonMessage(Activity activity, String title,
			String msg, String buttonTitle,
			DialogInterface.OnClickListener positiveListener) {
		if (isProgressDialogRunning) {
			hideProgressMessage();
		}
		mActivity = activity;
		AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
		builder.setMessage(Html.fromHtml(msg)).setTitle(title);
		builder.setPositiveButton(buttonTitle, positiveListener);

		AlertDialog dialog = builder.create();

		WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
		lp.dimAmount = 0.0f;
		dialog.getWindow().setAttributes(lp);
		dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

		try {
			dialog.show();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public boolean showTwoButtonMessageWithDoNotShowAgain(Activity activity,
			String title, String msg, View.OnClickListener positiveListener,
			View.OnClickListener negativeListener,
			final SharedPreferences sharedPrefs, final String prefKey) {

		boolean isForbidden = sharedPrefs.getBoolean(prefKey, false);
		if (isForbidden) {
			positiveListener.onClick(null);
			return true;
		}

		if (isProgressDialogRunning) {
			hideProgressMessage();
		}
		if (mDialog != null) {
			mDialog.cancel();
			mDialog = null;
		}
		mActivity = activity;
		mDialog = new Dialog(mActivity,
				android.R.style.Theme_Translucent_NoTitleBar);
		mDialog.setContentView(R.layout.dialog_dont_show);

		CheckBox checkBox = (CheckBox) mDialog.findViewById(R.id.skipCheck);
		TextView titleText = (TextView) mDialog.findViewById(R.id.titleView);
		TextView msgText = (TextView) mDialog.findViewById(R.id.messegeView);
		Button okButton = (Button) mDialog.findViewById(R.id.okButton);
		Button cancelButton = (Button) mDialog.findViewById(R.id.cancelButton);

		msgText.setText(Html.fromHtml(msg));
		titleText.setText(Html.fromHtml(title));

		checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				isForbiddenToAskAgain = isChecked;
			}
		});

		okButton.setOnClickListener(positiveListener);
		cancelButton.setOnClickListener(negativeListener);

		WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
		lp.dimAmount = 0.0f;
		mDialog.getWindow().setAttributes(lp);
		mDialog.getWindow()
				.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

		try {
			mDialog.show();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return false;
	}

	public boolean commitSharedPref(final SharedPreferences sharedPrefs,
			final String prefKey) {
		SharedPreferences.Editor editor = sharedPrefs.edit();
		editor.putBoolean(prefKey, isForbiddenToAskAgain);
		editor.commit();

		return isForbiddenToAskAgain;
	}

	public boolean isProgressDialogRunning() {
		return isProgressDialogRunning;
	}

}
