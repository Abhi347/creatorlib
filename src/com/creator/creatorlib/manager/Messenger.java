package com.creator.creatorlib.manager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.text.Html;
import android.view.WindowManager;
import android.widget.TextView;

import com.creator.creatorlib.R;

/* Use CreatorMessenger instead of Messenger to avoid name mangling with android.os.Messenger*/
@Deprecated
public class Messenger {
	private static Messenger mInstance = null;

	public static Messenger getInstance() {
		if (mInstance == null) {
			mInstance = new Messenger();
		}
		return mInstance;
	}

	private Activity mActivity;
	boolean isProgressDialogRunning = false;

	Dialog mDialog;
	TextView textView;

	public void showMessage(Activity activity, String title, String msg) {
		if (isProgressDialogRunning) {
			hideProgressMessage();
		}
		mActivity = activity;
		mDialog = null;
		AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
		builder.setMessage(Html.fromHtml(msg)).setTitle(title);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// User clicked OK button
				dialog.dismiss();
			}
		});

		AlertDialog dialog = builder.create();

		WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
		lp.dimAmount = 0.0f;
		dialog.getWindow().setAttributes(lp);
		dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

		try {
			dialog.show();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void showMessage(Activity activity, String title, String msg,
			String buttonTitle) {
		if (isProgressDialogRunning) {
			hideProgressMessage();
		}
		mActivity = activity;
		AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
		builder.setMessage(Html.fromHtml(msg)).setTitle(title);
		builder.setPositiveButton(buttonTitle,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// User clicked OK button
						dialog.dismiss();
					}
				});

		AlertDialog dialog = builder.create();

		WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
		lp.dimAmount = 0.0f;
		dialog.getWindow().setAttributes(lp);
		dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

		try {
			dialog.show();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void showProgressMessage(Activity activity, String msg) {
		if (isProgressDialogRunning) {
			hideProgressMessage();
		}
		isProgressDialogRunning = true;
		mActivity = activity;
		mDialog = new Dialog(mActivity,
				android.R.style.Theme_Translucent_NoTitleBar);
		// mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialog.setContentView(R.layout.dialog_loading);

		textView = (TextView) mDialog.findViewById(R.id.textView);
		textView.setText(Html.fromHtml(msg));

		WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
		lp.dimAmount = 0.5f;
		mDialog.getWindow().setAttributes(lp);
		mDialog.getWindow()
				.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

		mDialog.setCancelable(false);
		mDialog.setCanceledOnTouchOutside(false);
		try {
			mDialog.show();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void hideMessage() {
		if (mDialog != null)
			mDialog.dismiss();
	}

	public void hideProgressMessage() {
		isProgressDialogRunning = false;

		if (mDialog != null)
			mDialog.dismiss();
	}

	public void showTwoButtonMessage(Activity activity, String title,
			String msg, DialogInterface.OnClickListener positiveListener,
			DialogInterface.OnClickListener negativeListener) {
		if (isProgressDialogRunning) {
			hideProgressMessage();
		}
		mActivity = activity;
		AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
		builder.setMessage(Html.fromHtml(msg)).setTitle(title);
		builder.setPositiveButton("Ok", positiveListener);
		builder.setNegativeButton("Cancel", negativeListener);

		AlertDialog dialog = builder.create();

		WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
		lp.dimAmount = 0.0f;
		dialog.getWindow().setAttributes(lp);
		dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

		try {
			dialog.show();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void showOneButtonMessage(Activity activity, String title,
			String msg, String buttonTitle,
			DialogInterface.OnClickListener positiveListener) {
		if (isProgressDialogRunning) {
			hideProgressMessage();
		}
		mActivity = activity;
		AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
		builder.setMessage(Html.fromHtml(msg)).setTitle(title);
		builder.setPositiveButton(buttonTitle, positiveListener);

		AlertDialog dialog = builder.create();

		WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
		lp.dimAmount = 0.0f;
		dialog.getWindow().setAttributes(lp);
		dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

		try {
			dialog.show();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
