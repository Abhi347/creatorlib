package com.creator.creatorlib.manager;

import android.app.Activity;
import android.content.Context;
import android.content.CursorLoader;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;

public class UtilityManager {
	private static UtilityManager mInstance = null;

	public static UtilityManager getInstance() {
		if (mInstance == null) {
			mInstance = new UtilityManager();
		}
		return mInstance;
	}

	public static boolean DEBUG = false;

	public void init(Activity activity) {
		// See if we're a debug or a release build
		try {
			PackageInfo packageInfo = activity.getPackageManager()
					.getPackageInfo(activity.getPackageName(),
							PackageManager.GET_SIGNATURES);
			if (packageInfo.signatures.length > 0) {
				for (Signature signature : packageInfo.signatures) {
					String sign = new String(signature.toByteArray());
					// Log.d("Signature", sign);
					if (sign.contains("Android Debug")) {
						DEBUG = true;
						break;
					}
				}
			}
		} catch (NameNotFoundException e1) {
			e1.printStackTrace();
		}
	}

	public static void debugLog(String tag, String message) {
		if (DEBUG) {
			Log.d(tag, message);
		}
	}

	public static void debugLog(String message) {
		if (DEBUG) {
			Log.d("CreatorLib", message);
		}
	}

	public static String getRealPathFromURI(Context context, Uri contentUri) {
		String[] proj = { MediaStore.Images.Media.DATA };
		CursorLoader loader = new CursorLoader(context, contentUri, proj, null,
				null, null);
		Cursor cursor = loader.loadInBackground();
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	public static float getPxFromDP(Context context, float dp) {
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		float fpixels = metrics.density * dp;
		return (fpixels + 0.5f);
	}
}
