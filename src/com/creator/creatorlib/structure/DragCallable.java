package com.creator.creatorlib.structure;

public interface DragCallable {
	public void onDragStarted();
	public void onDragEntered();
	public void onDragExited();
	public void onDragDropped();
	public void onDragDropExited();
}
