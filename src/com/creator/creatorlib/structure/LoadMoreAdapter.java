package com.creator.creatorlib.structure;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class LoadMoreAdapter extends BaseAdapter{
	
	TextView textView;
	
	public LoadMoreAdapter(Context context){
		super();
		textView = new TextView(context);
		textView.setText("Load More...");
		
		textView.setTextColor(Color.BLACK);
		textView.setGravity(Gravity.CENTER);
		textView.setPadding(10, 10, 10, 10);
		textView.setTypeface(null, Typeface.BOLD);
	}
	
	public LoadMoreAdapter(Context context, String message){
		super();
		textView = new TextView(context);
		textView.setText(message);
		
		textView.setTextColor(Color.BLACK);
		textView.setGravity(Gravity.CENTER);
		textView.setPadding(10, 10, 10, 10);
		textView.setTypeface(null, Typeface.BOLD);
	}
	
	public LoadMoreAdapter(Context context, int viewResource){
		super();
		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		textView = (TextView) inflater.inflate(viewResource, null, false);
	}
	
	public LoadMoreAdapter(Context context, String message, int viewResource){
		super();
		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		textView = (TextView) inflater.inflate(viewResource, null, false);
		textView.setText(message);
	}

	@Override
	public int getCount() {
		return 1;
	}

	@Override
	public Object getItem(int position) {
		return textView;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public View getLoadingView(){
		return textView;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return textView;
	}

}
