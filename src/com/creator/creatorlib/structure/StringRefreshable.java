package com.creator.creatorlib.structure;

public class StringRefreshable implements Refreshable{
	private String mValue = null;

	public String getValue() {
		return mValue;
	}
	
	public StringRefreshable(String value){
		mValue = value;
	}
}
