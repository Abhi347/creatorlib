package com.creator.creatorlib.structure;

import android.view.DragEvent;
import android.view.View;
import android.view.View.OnDragListener;

public class ViewDragListener implements OnDragListener {
	int mEnterShape = 0;
	int mNormalShape = 0;
	DragCallable mDraggable;

	public void setBackgroundResource(int enterShape, int normalShape) {
		mEnterShape = enterShape;
		mNormalShape = normalShape;
	}

	public void setDragCallable(DragCallable draggable) {
		mDraggable = draggable;
	}

	public ViewDragListener(DragCallable draggable) {
		mDraggable = draggable;
	}

	private void changeToEnterBackground(View view) {

		if (mEnterShape == 0)
			return;
		int pL = view.getPaddingLeft();
		int pT = view.getPaddingTop();
		int pR = view.getPaddingRight();
		int pB = view.getPaddingBottom();

		view.setBackgroundResource(mEnterShape);
		view.setPadding(pL, pT, pR, pB);
	}

	private void changeToNormalBackground(View view) {

		if (mNormalShape == 0)
			return;
		int pL = view.getPaddingLeft();
		int pT = view.getPaddingTop();
		int pR = view.getPaddingRight();
		int pB = view.getPaddingBottom();

		view.setBackgroundResource(mNormalShape);
		view.setPadding(pL, pT, pR, pB);
	}

	@Override
	public boolean onDrag(View view, DragEvent event) {
		int action = event.getAction();
		switch (action) {
		case DragEvent.ACTION_DRAG_STARTED:
			mDraggable.onDragStarted();
			break;
		case DragEvent.ACTION_DRAG_ENTERED:
			changeToEnterBackground(view);
			mDraggable.onDragEntered();
			break;
		case DragEvent.ACTION_DRAG_EXITED:
			changeToNormalBackground(view);
			mDraggable.onDragExited();
			break;
		case DragEvent.ACTION_DROP:
			mDraggable.onDragDropped();
			break;
		case DragEvent.ACTION_DRAG_ENDED:
			changeToNormalBackground(view);
			mDraggable.onDragDropExited();
		default:
			break;
		}
		return true;
	}
}
