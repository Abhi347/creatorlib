package com.creator.creatorlib.sound;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.MediaStore;

public class CreatorSoundHelper {

	// Increases the toucharea of the view
	public static void setRingtone(Context context, String absolutePath, String title, String mimeType) {
		ContentResolver contentResolver = context.getContentResolver();
		ContentValues values = new ContentValues();
		values.put(MediaStore.MediaColumns.DATA, absolutePath);
		values.put(MediaStore.MediaColumns.TITLE, "title");
		values.put(MediaStore.MediaColumns.MIME_TYPE, "mimeType");
		// values.put(MediaStore.MediaColumns.SIZE, newSoundFile.length());
		//values.put(MediaStore.Audio.Media.ARTIST, R.string.app_name);
		values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
		values.put(MediaStore.Audio.Media.IS_NOTIFICATION, true);
		values.put(MediaStore.Audio.Media.IS_ALARM, true);
		values.put(MediaStore.Audio.Media.IS_MUSIC, false);

		Uri uri = MediaStore.Audio.Media.getContentUriForPath(absolutePath);
		Uri newUri = contentResolver.insert(uri, values);
		RingtoneManager.setActualDefaultRingtoneUri(context,
				RingtoneManager.TYPE_RINGTONE, newUri);

	}
}
