package com.creator.creatorlib.view;

import android.content.ClipData;
import android.graphics.Rect;
import android.view.TouchDelegate;
import android.view.View;
import android.view.View.DragShadowBuilder;

import com.creator.creatorlib.structure.DragCallable;
import com.creator.creatorlib.structure.ViewDragListener;

public class CreatorViewHelper {

	// Increases the toucharea of the view
	public static void increaseTouchArea(View delegateView, float touchAddition) {
		try {
			TouchDelegate delegate = new TouchDelegate(new Rect(0, 0,
					(int) (delegateView.getWidth() + touchAddition),
					(int) (delegateView.getHeight() + touchAddition)),
					delegateView);
			delegateView.setTouchDelegate(delegate);
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
	}
	
	public static void makeDraggable(final View button, int bgEnter, int bgNormal, final boolean useLongClick) {
		DragCallable draggable = new DragCallable() {

			@Override
			public void onDragStarted() {
			}

			@Override
			public void onDragExited() {
			}

			@Override
			public void onDragEntered() {
			}

			@Override
			public void onDragDropped() {
				if(useLongClick){
					button.performLongClick();
				}else
					button.performClick();
			}

			@Override
			public void onDragDropExited() {
			}
		};

		ViewDragListener dragListener = new ViewDragListener(draggable);

		if(bgEnter != 0 || bgNormal!=0)
			dragListener.setBackgroundResource(bgEnter, bgNormal);

		button.setOnDragListener(dragListener);
	}

	public static void startDrag(View view){
		ClipData data = ClipData.newPlainText("", "");
		DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
				view);
		view.startDrag(data, shadowBuilder, view, 0);
	}
}
